/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the Uno and
  Leonardo, it is attached to digital pin 13. If you're unsure what
  pin the on-board LED is connected to on your Arduino model, check
  the documentation at http://arduino.cc

  This example code is in the public domain.

  modified 8 May 2014
  by Scott Fitzgerald
  
  Modified by Roger Clark. www.rogerclark.net for Maple mini 25th April 2015 , where the LED is on PB1
  
 */

 #include <Wire.h>
#include <HardWire.h>

HardWire wire1(1 /*2*/, 0);
HardWire wire2(2 /*2*/, 0);

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin PB1 as an output.
  pinMode(PC13, OUTPUT);
//  Serial.begin(9600);

//   wire = new HardWire(I2C1, 0);
   wire1.begin(28); // intitalize this device as a slave on addr .
   wire2.begin(28); // intitalize this device as a slave on addr .
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(PC13, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);// wait for a second

  if (wire1.available()>0)
  {
    uint8 data = wire1.read();
//    Serial.write("Received i2c  === %d\n ", (int)data);
//    Serial.write("available %d", (int)wire.available());
    for (int i = 0; i!=data; ++i)
    {
    delay(200);              
    digitalWrite(PC13, LOW);    // turn the LED off by making the voltage LOW
    delay(200);              
    digitalWrite(PC13, HIGH);  
    }
    delay(2000);              
  }

  
  if (wire2.available()>0)
  {
    uint8 data = wire2.read();

    for (int i = 0; i!=data; ++i)
    {
    delay(200);              
    digitalWrite(PC13, LOW);    // turn the LED off by making the voltage LOW
    delay(200);              
    digitalWrite(PC13, HIGH);  
    }
    delay(2000);              
}

  
    digitalWrite(PC13, HIGH);   
    delay(100);  
//    Serial.write("hello to you\n");
     
}
